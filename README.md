# Cinnamon Eyes

An extension that adds an eye to the panel bar that follows your cursor.
Currently, it is being maintained at [anaximeno/c-eyes](https://github.com/anaximeno/c-eyes). This here is an older fork.

### Contributors

<!-- NOTE: If you did contribute to this applet, you can add your github username as a link to you profile address (optional) in the list bellow. Please add your name before the last one `et upstream`. -->

- [anaximeno](https://github.com/anaximeno), [Drugwash2](https://github.com/Drugwash2), [et upstream](https://github.com/alexeylovchikov/eye-extended-shell-extension/graphs/contributors)


### Translators

<!-- NOTE: If you did help translating this applet, you can add your github username as a link to you profile address (optional) in the list bellow. -->

- 


#### Relevant Documentation for Contribution

* http://smasue.github.io/gnome-shell-tw
* https://github.com/optimisme/gjs-examples
* https://projects.linuxmint.com/reference/git/cinnamon-tutorials/xlet-settings-ref.html
* https://projects.linuxmint.com/reference/git/cinnamon-tutorials/xlet-settings.html
